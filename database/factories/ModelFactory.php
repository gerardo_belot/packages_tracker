<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password = bcrypt('secret'),
        'status_id' => 1,
        'enterprice_id' => factory(\App\Enterprice::class)->create(),
        'remember_token' => str_random(10),
    ];
});

/**
 * Enterprice
 */
$factory->define(App\Enterprice::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->sentence(),
        'status_id' => 1
    ];
});


/**
 * Roles
 */
$factory->define(App\Role::class, function (Faker\Generator $faker){
    return [
        'name' => $faker->name,
        'display_name' => $faker->name,
        'description' => $faker->sentence
    ];
});

/**
 * Permissions
 */
$factory->define(App\Permission::class, function (Faker\Generator $faker){
    return [
        'name' => $faker->name,
        'display_name' => $faker->name,
        'description' => $faker->sentence
    ];
});
