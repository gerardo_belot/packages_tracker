<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterprice extends Model
{
    protected $fillable = ['name', 'status_id'];
}
