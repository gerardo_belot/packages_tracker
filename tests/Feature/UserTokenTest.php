<?php

namespace Tests\Feature;

use App\User;
use Faker\Factory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTokenTest extends TestCase
{

    use DatabaseTransactions, WithoutMiddleware;


    /**
     * @test
     */
    public function api_path_authenticate_and_return_token()
    {

        $user = factory(User::class)->create();

        $data = [
            'email' => $user->email,
            'password' => 'secret'
        ];

        $response = $this->json('POST', '/api/auth/', $data);

        $response->assertResponseStatus(200);
    }


}
