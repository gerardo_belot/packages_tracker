<?php

namespace Tests\Feature;

use App\Enterprice;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class UserControllerTest extends TestCase
{

    use DatabaseTransactions, WithoutMiddleware;

    /**
     * @test
     */

    public function userController_find_a_user_by_id()
    {
        $user = factory(User::class)->create();

        $request = $this->json('GET', '/api/user/', ['id' => $user->id]);
        $request->assertResponseStatus(200);

        $request->assertResponseStatus(200);
        $json = json_encode(['name' => $user->name]);
        $request->assertJson($json);
    }

    /**
     * @test
     */
    public function userController_can_edit_a_use_by_id()
    {
        $user = factory(User::class)->create();

        $input = [
            'id' => $user->id,
            'name' => 'gerardo'
        ];



        $request = $this->json('PUT', '/api/user/' . $user->id, $input);
        $json = json_encode(['name' => 'gerardo']);

        $request->assertResponseStatus(200);
        $this->seeInDatabase('users', ['name' => 'gerardo']);
        $request->assertJson($json);
    }

    /**
     * @test
     */
    function userController_can_create_user_from_form()
    {
        $enterprice = factory(Enterprice::class)->create();

        $input = [
            'name' => 'gerardo',
            'email' => 'gbelot@hotmail.com',
            'password' => 'Luna0102',
            'enterprice_id' => $enterprice->id,
            'password_confirmation' => 'Luna0102',
            'status_id' => 1
        ];

        $request = $this->json('POST', '/api/user', $input);

        $request->assertResponseStatus(200);
        $this->seeInDatabase('users', ['name' => 'gerardo']);
    }

    /**
     * @test
     */
    function userController_can_list_of_users()
    {
        factory(User::class, 10)->create();

        $request= $this->json('GET', '/api/user');


        $request->assertResponseStatus(200);
        $this->seeJsonStructure([
            'current_page', 'from', 'last_page', 'next_page_url', 'path', 'per_page', 'prev_page_url', 'to', 'total',
                'data' => [
                    '*' => ['id', 'name', 'email', 'status_id']
                ]
        ]);
    }

}



























