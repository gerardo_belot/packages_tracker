<?php

namespace Tests\Unit;

use App\Enterprice;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test de modelo de usuarios
     * No deberia testear user, password porque este comportamiento ya esta testeado,
     * deberia consentrarme en las nuevas featurs como relaciones y estado del usuario
     * @return void
     * @test
     */
    public function a_user_can_be_created()
    {
        /**
         * Confirmamos que usuario puede ser creado y testeado correctamente
         */
        $user = factory(User::class)->create(['email' => 'gbelot@yahoo.com']);
        $this->assertEquals($user->email, 'gbelot@yahoo.com');
    }

    /**
     * @test
     */
    public function a_user_have_a_status()
    {
        /**
         * Confirmamos que status_id esta correctamente agregado y funcional
         */
        $user = factory(User::class)->create(['status_id' => 2]);
        $this->assertEquals($user->status_id, 2);
    }

    /**
     * @test
     */
    public function a_user_have_a_enterprice_relationship()
    {
        /**
         * Confirmamos que un usuario pertenece a una empresa
         */
        $enterprice = factory(Enterprice::class)->create();

        $user = factory(User::class)->create(['enterprice_id' => $enterprice->id]);

        $this->assertEquals($user->enterprice->name, $enterprice->name);
    }
}
