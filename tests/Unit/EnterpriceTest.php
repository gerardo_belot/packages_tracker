<?php

namespace Tests\Unit;

use App\Enterprice;
use Faker\Factory;
use Illuminate\Support\Facades\App;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EnterpriceTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     * @test
     */
    public function an_enterprice_have_a_name_and_status_id()
    {
        /**
         * Created the Enterprice by Factory, given name and status_id
         */
        $enterprice = factory(Enterprice::class)->create(['name' => 'Empresa1', 'status_id' => 2]);

        $this->assertEquals('Empresa1', $enterprice->name);
        $this->assertEquals(2, $enterprice->status_id);
    }
}
